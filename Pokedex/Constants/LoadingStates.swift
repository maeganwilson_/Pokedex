//
//  LoadingStates.swift
//  Pokedex
//
//  Created by Maegan Wilson on 6/14/21.
//

import Foundation

public enum LoadingState {
    case idle
    case loading
    case error(Error)
    case loaded
}
