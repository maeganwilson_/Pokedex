//
//  PokedexApp.swift
//  Pokedex
//
//  Created by Maegan Wilson on 6/13/21.
//

import SwiftUI

@main
struct PokedexApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
