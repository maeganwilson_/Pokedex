//
//  PokeListLoading.swift
//  Pokedex
//
//  Created by Maegan Wilson on 6/14/21.
//

import SwiftUI

struct PokeRowLoading: View {
    var body: some View {
        HStack(alignment: .top){
            VStack{
                Text("#0025")
                    .redacted(reason: .placeholder)
                Image(systemName: "star")
                    .redacted(reason: .placeholder)
            }
            VStack(alignment: .leading){
                Text("Charmeleon")
                    .font(.headline)
                    .redacted(reason: .placeholder)
                Spacer()
                TypeChipList(types: [PokemonType(slot: 0, type: APIResource(name: "flying", url: "https://pokeapi.co/api/v2/type/3")), PokemonType(slot: 1, type: APIResource(name: "flying", url: "https://pokeapi.co/api/v2/type/3")), PokemonType(slot: 2, type: APIResource(name: "flying", url: "https://pokeapi.co/api/v2/type/3"))])
                    .redacted(reason: .placeholder)
            }
            Spacer()
        }
    }
}
struct PokeListLoading: View {
    let numbers = 0...25
    var body: some View {
        List(numbers, id: \.self){ number in
            PokeRowLoading()
        }
    }
}

struct PokeListLoading_Previews: PreviewProvider {
    static var previews: some View {
        PokeListLoading()
    }
}
