//
//  PokeList.swift
//  Pokedex
//
//  Created by Maegan Wilson on 6/14/21.
//

import SwiftUI

struct PokeList: View {
    @State private var loadingState = "idle"
    var pokemons: [PokemonAndImage] = []
    
    var body: some View {
        List(pokemons, id: \.info.name){ poke in
            PokeRow(info: poke.info, Sprite: poke.image)
        }.listStyle(.insetGrouped)
    }
}
