//
//  PokeRow.swift
//  Pokedex
//
//  Created by Maegan Wilson on 6/14/21.
//

import SwiftUI

struct PokeRow: View {
    var info: Pokemon
    var Sprite: Image
    
    var body: some View {
        HStack(alignment: .top){
            VStack{
<<<<<<< HEAD
                Text("#\(info.id) \(info.name.capitalized)")
                    .font(.headline)
                AsyncImage(url: URL(string: info.sprites.front_default!))
=======
                Text("#\(info.id)")
                AsyncImage(url: URL(string: info.sprites.front_default!))
            }
            VStack(alignment: .leading){
                Text("\(info.name.capitalized)")
                    .font(.headline)
                Spacer()
                TypeChipList(types: info.types)
>>>>>>> dev
            }
            Spacer()
        }
    }
}

//struct PokeRow_Previews: PreviewProvider {
//    static var previews: some View {
//        PokeRow()
//    }
//}
