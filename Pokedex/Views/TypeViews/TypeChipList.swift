//
//  TypeChipList.swift
//  Pokedex
//
//  Created by Maegan Wilson on 6/13/21.
//

import SwiftUI

struct TypeChipList: View {
    var types: [PokemonType]
    
    let columns = [GridItem(.adaptive(minimum: 30, maximum: 70))]
    
    var body: some View {
            LazyHGrid(rows: columns, spacing: 10) {
                ForEach(types, id: \.slot) { item in
                    TypeChip(name: item.type.name, color: getColorFor(type: item.type.name))
                }
            }
    }
    
    private func getColorFor(type: String) -> Color{
        switch type {
        case "fire":
            return .red
        case "grass":
            return .green
        case "poison":
            return .purple
        case "water":
            return .blue
        case "electric":
            return .yellow
        default:
            return .gray
        }
    }
}
