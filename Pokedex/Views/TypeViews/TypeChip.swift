//
//  TypeChip.swift
//  Pokedex
//
//  Created by Maegan Wilson on 6/13/21.
//

import SwiftUI

struct TypeChip: View {
    var name: String
    var color: Color
    
    var body: some View {
        ZStack{
            RoundedRectangle(cornerRadius: 8.0)
                .fill(color)
            HStack{
                Text(name.localizedCapitalized)
                    .font(.footnote)
                    .padding(4)
                    .background(
                        .thinMaterial,
                        in: RoundedRectangle(cornerRadius: 8.0)
                    )
            }.padding(4)
        }
    }
}

struct TypeChip_Previews: PreviewProvider {
    static var previews: some View {
        TypeChip(name: "water", color: .blue)
            .frame(width: 70, height: 40)
    }
}
