//
//  APIResult.swift
//  Pokedex
//
//  Created by Maegan Wilson on 6/13/21.
//

import Foundation

struct APIResource: Codable {
    var name: String
    var url: String
}
