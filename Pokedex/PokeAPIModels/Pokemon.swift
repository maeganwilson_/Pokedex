//
//  Pokemons.swift
//  Pokedex
//
//  Created by Maegan Wilson on 6/13/21.
//

import Foundation
import SwiftUI

struct PokemonAndImage {
    var image: Image
    var info: Pokemon
}

struct Pokemons: Codable {
    var count: Int
    var next: String?
    var previous: String?
    var results: [APIResource]
}

struct Pokemon: Codable {
    var id: Int
    var name: String
    var base_experience: Int
    var height: Int
    var is_default: Bool
    var order: Int
    var weight: Int
    var abilities: [PokemonAbility]
    var forms: [APIResource]
    var game_indicies: [VersionGameIndex]?
    var held_items: [PokemonHeldItem]
    var location_area_encounters: String
    var moves: [PokemonMove]
    var sprites: PokemonSprites
    var species: APIResource
    var stats: [PokemonStat]
    var types: [PokemonType]
    
}

struct PokemonAbility: Codable {
    var is_hidden: Bool
    var slot: Int
    var ability: APIResource
}

struct PokemonHeldItem: Codable {
    var item: APIResource
    var version_details: [PokemonHeldItemVersion]
}

struct PokemonHeldItemVersion: Codable {
    var version: APIResource
    var rarity: Int
}

struct PokemonMove: Codable {
    var move: APIResource
    var version_group_details: [PokemonMoveVersion]
}

struct PokemonMoveVersion: Codable {
    var move_learn_method: APIResource
    var version_group: APIResource
    var level_learned_at: Int
}

struct PokemonSprites: Codable {
    var front_default: String?
    var front_shiny: String?
    var front_female: String?
    var front_shiny_female: String?
    var back_default: String?
    var back_shiny: String?
    var back_female: String?
    var back_shiny_female: String?
}

struct PokemonStat: Codable {
    var stat: APIResource
    var effort: Int
    var base_stat: Int
}

struct PokemonType: Codable {
    var slot: Int
    var type: APIResource
}
