//
//  Utility.swift
//  Pokedex
//
//  Created by Maegan Wilson on 6/13/21.
//

import Foundation

struct VersionGameIndex: Codable {
    var game_index: Int
    var version: APIResource
}

struct GenerationGameIndex: Codable {
    var game_index: Int
    var version: APIResource
}

struct PokeName: Codable {
    var name: String
    var language: APIResource
}
