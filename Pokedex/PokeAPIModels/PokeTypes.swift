//
//  PokeTypes.swift
//  Pokedex
//
//  Created by Maegan Wilson on 6/13/21.
//

import Foundation

struct PokeType: Codable {
    var id: Int
    var name: String
    var damage_relations: TypeRelations
    var game_indicies: [GenerationGameIndex]
    var generation: APIResource
    var move_damage_class: APIResource
    var names: [PokeName]
    var pokemon: [TypePokemon]
    var moves: [APIResource]
}

struct TypeRelations: Codable {
    var no_damage_to: [APIResource]
    var half_damage_to: [APIResource]
    var double_damage_to: [APIResource]
    var no_damage_from: [APIResource]
    var half_damage_from: [APIResource]
    var double_damage_from: [APIResource]
}

struct TypePokemon: Codable {
    var slot: Int
    var pokemon: APIResource
}
