//
//  PokeAPIService.swift
//  Pokedex
//
//  Created by Maegan Wilson on 6/13/21.
//

import Foundation
import SwiftUI

class PokeAPIService {
    public static let shared = PokeAPIService()
    
    private init() {}
    private let urlSession = URLSession.shared
    private let baseURL = URL(string: "https://pokeapi.co/api/v2")!
    
    private let jsonDecoder: JSONDecoder = {
        let jsonDecoder = JSONDecoder()
        return jsonDecoder
    }()
    
    enum Endpoint: String, CaseIterable {
        case pokemon
        case games
    }
    
    enum PokeAPIServiceError: Error {
        case apiError
        case invalidEndpoint
        case invalidResponse
        case noData
        case decodeError
    }
    
    public func fetchPokemons(from endpoint: Endpoint) async throws -> Pokemons {
        let url = baseURL.appendingPathComponent(endpoint.rawValue)
        let (data, _) = try await urlSession.data(from: url)
        let response = try jsonDecoder.decode(Pokemons.self, from: data)
        return response
    }
    
    public func fetchPokemonInfo(from url: String) async throws -> Pokemon {
        let pokeURL = URL(string: url)!
        let (data, _) = try await urlSession.data(from: pokeURL)
        let response = try jsonDecoder.decode(Pokemon.self, from: data)
        return response
    }
    
    public func fetchPokeImage(from url: String)  async throws -> Image {
        let imageURL = URL(string: url)!
        let (data, _) = try await urlSession.data(from: imageURL)
        let image: UIImage = UIImage(data: data)!
        return Image(uiImage: image)
    }
    
    public func fetchTypeInfo(from url: String) async throws -> PokeType {
        let typeURL = URL(string: url)!
        let (data, _) = try await urlSession.data(from: typeURL)
        let response = try jsonDecoder.decode(PokeType.self, from: data)
        return response
    }
    
}
