//
//  ContentView.swift
//  Pokedex
//
//  Created by Maegan Wilson on 6/13/21.
//

import SwiftUI

struct ContentView: View {
    @State private var loadingState: LoadingState = .idle
    @State private var pokemons: [PokemonAndImage] = []
    var body: some View {
        switch loadingState {
        case .idle:
            Color.clear.onAppear(perform: loadPokes)
        case .loading:
            PokeListLoading()
        case .error(let error):
            Text(error.localizedDescription)
        case .loaded:
            PokeList(pokemons: pokemons)
                .refreshable {
                    loadPokes()
                }
        }
    }
    
    private func loadPokes() {
        loadingState = .loading
        async {
            do {
                let tempPokemons = try await PokeAPIService.shared.fetchPokemons(from: .pokemon)
                for poke in tempPokemons.results {
                    let tempPokemon = try await PokeAPIService.shared.fetchPokemonInfo(from: poke.url)
                    guard let imageURL = tempPokemon.sprites.front_default else {
                        return
                    }
                    let img = try await PokeAPIService.shared.fetchPokeImage(from: imageURL)
                    let pokemonToAdd = PokemonAndImage(image: img,
                                                       info: tempPokemon)
                    pokemons.append(pokemonToAdd)
                }
                loadingState = .loaded
            } catch {
                loadingState = .error(error)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
